package info.bitcoinunlimited.votepeer

import android.util.Log
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.vote.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.utils.TAG_ELECTRUM_API
import java.lang.Thread.sleep
import java.net.UnknownHostException
import java.util.Random
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.scheduleAtFixedRate
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectrumAPI(private val chain: ChainSelector) {

    val connectionState = MutableStateFlow<ElectrumApiConnectionState>(ElectrumApiConnectionState.Connecting("????", 1337))
    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_ELECTRUM_API, exception.message ?: "Something went wrong in ElectrumAPi!")

        connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception(exception))
    }

    private var cli: ElectrumClient? = null
    private var cliMutex = Mutex()
    private var bestBlockHeader: BlockHeader? = null
    private var bestBlockHeaderMutex: Mutex = Mutex()

    // Only cache transactions <= 1000 bytes. At worst case, 300 elements takes 0.3MB of ram.
    private val maxTxCacheElementSize = 1000
    private var maxTxCacheElements = 300
    private var txCache: MutableMap<Hash256, ByteArray> = mutableMapOf()
    private val txRng = Random()

    // For coRoutine Errors
    val exception: MutableStateFlow<Exception?> = MutableStateFlow(null)

    /**
     * Task for sending ping request to the server at 1 minute interval.
     */
    private var pingTask: TimerTask? = null
    private val electrumServers: Array<Pair<String, Int>> = arrayOf(
        Pair("electrs.bitcoinunlimited.info", 50002),
        Pair("bitcoincash.network", 50002),
        Pair("bitcoincash.network", 50001, /* no ssl */),
    )

    /**
     * Reset client such that a new connection will be attempted at next API call.
     *
     * Requires cliMutex lock.
     */
    private fun resetConnection() {
        cli?.stop()
        cli = null
        Log.i(TAG_ELECTRUM_API, "Resetting connection.")
    }

    /**
     * Attempt connection to electrum servers. Tries multiple servers.
     *
     * Requires cliMutex lock.
     *
     * Returns false if we were already connected, true if we made a new connection. On error, returns false. The property connectionState
     * contains the error.
     */
    // TODO: move to suspendCoroutine?
    suspend fun connect(): Boolean {
        if (cli != null) {
            return false
        }
        if (chain != ChainSelector.BCHMAINNET) {
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception("NYI. Only BCH mainnet is supported."))
            return false
        }

        pingTask = null

        for ((host, port) in electrumServers) {
            try {
                connectionState.value = ElectrumApiConnectionState.Connecting(host, port)
                Log.i(TAG_ELECTRUM_API, "Connecting to $host:$port ...")
                cli = ElectrumClient(chain, host, port)
                cli!!.start()
                val version = cli!!.version().toString()
                Log.i(TAG_ELECTRUM_API, "Connected to $host:$port. Server version $version")
                connectionState.value = ElectrumApiConnectionState.Connected(host, port)
                break
            } catch (e: java.net.SocketTimeoutException) {
                Log.w(TAG_ELECTRUM_API, "Electrum timeout: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                resetConnection()
                continue
            } catch (e: java.net.ConnectException) {
                Log.w(TAG_ELECTRUM_API, "Electrum connection error: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                resetConnection()
                continue
            } catch (e: UnknownHostException) {
                Log.w(TAG_ELECTRUM_API, "Electrum UnknownHostException: $e")
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(e)
                resetConnection()
                continue
            } catch (e: ElectrumRequestTimeout) {
                val message = "Connected to $host:$port, but server did not respond to version request. Disconnecting."
                Log.w(TAG_ELECTRUM_API, message)
                connectionState.value = ElectrumApiConnectionState.ElectrumApiError(Exception(message))
                resetConnection()
            }
        }

        if (cli == null) {
            val exception = Exception("Failed to connect to electrum servers.")
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
            return false
        }

        // Keep connection alive sending ping request at 1 minute interval.
        pingTask = Timer("electrum ping", true).scheduleAtFixedRate(100, (60 * 1000).toLong()) {
            GlobalScope.launch(Dispatchers.IO + handler) { this@ElectrumAPI.ping() }
        }

        return true
    }

    /**
     * Waits for bestBlockHeader property to be set. Throws ElectrumRequestTimeout on timeout (~10 seconds).
     */
    private suspend fun waitForTip() {
        var attempts = 0
        while (true) {
            if (attempts++ > 200) {
                Log.w(TAG_ELECTRUM_API, "Failed to receive initial block header from electrum server")
                throw ElectrumRequestTimeout()
            }
            bestBlockHeaderMutex.withLock {
                if (bestBlockHeader != null) {
                    return
                }
            }
            sleep(50)
        }
    }

    // requires cliMutex lock
    private suspend fun startSubscriptions() {
        if (cli == null) {
            throw Error("Connection required for initiating subscription")
        }
        // Subscribe to block header
        cli?.let {
            it.subscribeHeaders { header ->
                GlobalScope.launch(Dispatchers.IO + handler) {
                    bestBlockHeaderMutex.withLock {
                        bestBlockHeader = header
                    }
                }
            }
        }
    }

    fun subscribeToAddress(address: String, onAddress: (status: String?) -> Unit) {
        cli?.let {
            it.subscribeAddressStatus(address) { status ->
                onAddress(status)
            }
        }
    }

    fun unsubscribeAddress(address: String) {
        cli?.let {
            it.unsubscribeAddress(address)
        }
    }

    /**
     * Calls a electrum client method. Connects if disconnected.
     */
    private suspend fun <T> withCli(action: suspend (ElectrumClient) -> T): T {
        cliMutex.withLock {
            for (attempt in 1..10) {
                try {
                    val connected = connect()
                    if (connected) {
                        startSubscriptions()
                    }
                    waitForTip()
                    return action(cli!!)
                } catch (e: ElectrumRequestTimeout) {
                    val exception = Exception("Electrum request attempt #$attempt timed out. Re-connecting.")
                    connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
                    resetConnection()
                } catch (exception: Exception) {
                    connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
                    resetConnection()
                    throw exception
                }
            }
            val exception = Exception("Failed to submit request to electrum server")
            connectionState.value = ElectrumApiConnectionState.ElectrumApiError(exception)
            throw exception
        }
    }

    suspend fun getOneUtxo(source: PayAddress, secret: Secret?, minimumValue: Long): BCHspendable? {
        for (unspent in listUnspent(source)) {
            if (unspent.amount < minimumValue) {
                continue
            }
            unspent.secret = secret
            return unspent
        }
        return null
    }

    suspend fun getTransactionHistory(scriptHash: BCHscript): Array<Pair<Int, Hash256>> {
        /**
         * Get server version
         */
        return withCli {
            return@withCli it.getHistory(scriptHash)
        }
    }

    /**
     * Find a utxo from `destinationAddress` that with at least specified amount of satoshis.
     * If non exist, create one from funds in `fundingAddress` and return it.
     */
    suspend fun getOrCreateUtxo(
        destinationAddress: PayAddress,
        fundingAddress: PayAddress,
        minimumSatoshis: Long,
        fundingSecret: Secret,
        destinationSecret: Secret?
    ): BCHspendable {
        // Check if destination already has a spendable utxo.
        getOneUtxo(destinationAddress, destinationSecret, minimumSatoshis)?.let {
            return it
        }

        // Fund destination from funding address.
        // TODO: Support multiple inputs
        val fundingTxFee = 270
        val newMinimum = minimumSatoshis + fundingTxFee
        val utxo = getOneUtxo(fundingAddress, fundingSecret, newMinimum) ?: throw Error("Not enough funds")

        val addChange = utxo.amount >= newMinimum + (NetworkConstants.DEFAULT_DUST_THRESHOLD + NetworkConstants.P2PKH_OUTPUT_SIZE)

        val tx = BCHtransaction(chain)
        tx.inputs.add(BCHinput(chain, utxo, BCHscript(chain)))

        // Output to destination address
        val voteOutput = BCHoutput(chain)
        voteOutput.amount = if (addChange) minimumSatoshis else utxo.amount - fundingTxFee
        voteOutput.script = destinationAddress.outputScript()
        tx.outputs.add(voteOutput)

        // Change output
        if (addChange) {
            val changeOutput = BCHoutput(chain)
            changeOutput.amount = utxo.amount - newMinimum
            changeOutput.script = fundingAddress.outputScript()
            tx.outputs.add(changeOutput)
        }

        // Sign funding transaction
        val pubkey = PayDestination.GetPubKey(fundingSecret.getSecret())
        val sig = UtilVote.signInput(tx, 0)

        // Assumes P2PKH input
        tx.inputs[0].script = BCHscript(chain, OP.push(sig), OP.push(pubkey))

        if (tx.feeRate < NetworkConstants.MIN_TX_FEE_RATE) {
            throw IllegalStateException("Funding transaction has too low fee rate ${tx.feeRate} < ${NetworkConstants.MIN_TX_FEE_RATE})")
        }

        val txid = tx.calcHash().toString()
        val txidReturned = broadcast(tx)
        Log.i(TAG_ELECTRUM_API, "$destinationAddress funded in tx $txid")
        if (txidReturned != txid) {
            // Warn about this, but trust our own calculated ID.
            Log.w(TAG_ELECTRUM_API, "Expected fund tx id $txid, but electrum returned $txidReturned")
        }
        val newCoin = BCHspendable(chain, tx.hash, 0, tx.outputs[0].amount)
        newCoin.priorOutScript = tx.outputs[0].script
        newCoin.secret = destinationSecret
        return newCoin
    }

    /**
     * Get server version
     */
    suspend fun version(): Pair<String, String> {
        return withCli {
            return@withCli it.version()
        }
    }

    /**
     * Get spendable coins for this destination.
     *
     * The secret in destination object will be passed on to the
     * BCHspendable instances.
     */
    suspend fun listUnspent(destination: PayDestination): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(destination)
        }
    }

    /**
     * Gets spendable coins for this destination.
     *
     * The BCHspendable instances will not contain the private key to spend the coins.
     */
    suspend fun listUnspent(address: PayAddress): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(address)
        }
    }

    /**
     * Get balance
     * **/
    suspend fun getBalance(address: PayAddress): ElectrumClient.BalanceResult {
        return withCli {
            return@withCli it.getBalance(address)
        }
    }

    /**
     * Broadcast a transaction
     */
    suspend fun broadcast(tx: BCHtransaction): String {
        return withCli {
            return@withCli it.sendTx(tx.BCHserialize(SerializationType.NETWORK).flatten())
        }
    }

    /**
     * Fetch a transaction
     */
    suspend fun getTransaction(txid: Hash256): BCHtransaction {
        val cachedTx: BCHtransaction? = getTxCache(txid)
        cachedTx?.let {
            Log.i(TAG_ELECTRUM_API, "cache hit for tx $txid")
            return cachedTx
        }
        return withCli {
            val tx = it.getTx(txid)
            putTxCache(tx)
            return@withCli tx
        }
    }

    private fun getTxCache(txid: Hash256): BCHtransaction? {
        val serialized = txCache.get(txid)
        serialized?.let {
            return BCHtransaction(chain, it, SerializationType.NETWORK)
        }
        return null
    }

    private fun putTxCache(tx: BCHtransaction) {
        if (txCache.size >= maxTxCacheElements) {
            // evict a random transaction
            val toEvict = txCache.entries.elementAt(txRng.nextInt(txCache.size))
            txCache.remove(toEvict.key)
        }
        // the serialized version is more optimized.
        val serialized = tx.BCHserialize(SerializationType.NETWORK).flatten()
        if (serialized.size <= maxTxCacheElementSize) {
            txCache[tx.hash] = serialized
        }
    }

    suspend fun ping() {
        return withCli {
            return@withCli it.ping()
        }
    }

    /**
     * Get the chain tip
     */
    internal suspend fun getBestBlockHeader(): BlockHeader? {
        bestBlockHeaderMutex.withLock {
            return bestBlockHeader
        }
    }

    suspend fun getLatestBlockHeight() = getBestBlockHeader()?.height

    fun isInMempool(confirmHeight: Long): Boolean {
        return ElectrumAPI.isInMempool(confirmHeight)
    }

    /**
     * Get transactions that *spend* from the given address.
     *
     * Returns list of height, tx
     *
     * Height is 0 or -1 if the transaction is unconfirmed.
     */
    suspend fun getSpendingTransactions(
        contractAddress: PayAddress,
        beginHeight: Long?,
        endHeight: Long?
    ): List<Pair<Long, BCHtransaction>> {
        val txs = mutableListOf<Pair<Long, BCHtransaction>>()

        val tipHeight = withCli { _ ->
            val tipHeight = getBestBlockHeader()
            if ((endHeight != null || beginHeight != null) && tipHeight == null) {
                error("blockchain tip is not available")
            }
            tipHeight?.height
        }

        if (beginHeight != null && tipHeight!! < beginHeight) {
            // Blockchain tip has not reached begin height. No need to query for transactions.
            return listOf()
        }

        val scripthash = contractAddress.outputScript().scriptHash()

        val history = withCli { cli ->
            cli.getHistory(scripthash)
        }
        for (h in history) {
            if (endHeight != null) {
                if (isInMempool(h.first.toLong())) {
                    if (tipHeight!! > endHeight) {
                        // tx won't confirm before maxHeight
                        continue
                    }
                } else if (h.first > endHeight) {
                    continue
                } else { /* ok, include tx */ }
            }
            if (beginHeight != null) {
                if (!isInMempool(h.first.toLong()) && h.first < beginHeight) {
                    // tx confirmed before beginHeight
                    continue
                }
            }
            val tx = getTransaction(h.second)
            if (hasInputFrom(tx, contractAddress)) {
                txs.add(Pair(h.first.toLong(), tx))
            }
        }
        return txs.toList()
    }

    /**
     * Fetch transaction *funding* an address. Optionally filter by minimum height (inclusive) and/or maximum height (inclusive)
     */
    @Suppress("ControlFlowWithEmptyBody")
    suspend fun getFundingTransactions(address: PayAddress, minHeight: Long?, maxHeight: Long?): List<Pair<Long, BCHtransaction>> {
        val tipHeight = withCli { _ ->
            val tipHeight = getBestBlockHeader()
            if ((maxHeight != null || minHeight != null) && tipHeight == null) {
                error("blockchain tip is not available")
            }
            tipHeight?.height
        }

        val txs = mutableListOf<Pair<Long, BCHtransaction>>()

        val outputScript = address.outputScript()
        val scripthash = outputScript.scriptHash()

        val history = withCli { cli ->
            cli.getHistory(scripthash)
        }
        for (h in history) {
            if (minHeight != null) {
                if (isInMempool(h.first.toLong())) {
                    if (tipHeight!! < minHeight) {
                        // tx might get confirmed before minimum height
                        continue
                    }
                } else if (h.first < minHeight) {
                    continue
                } else { /* include tx */ }
            }
            if (maxHeight != null) {
                if (isInMempool(h.first.toLong())) {
                    if (tipHeight!! > maxHeight) {
                        // tx won't confirm before maxHeight
                        continue
                    }
                } else if (h.first > maxHeight) {
                    continue
                } else { /* ok, include tx */ }
            }
            val tx = getTransaction(h.second)
            if (hasOutputTo(tx, outputScript)) {
                txs.add(Pair(h.first.toLong(), tx))
            }
        }
        return txs.toList()
    }

    private fun hasOutputTo(tx: BCHtransaction, outputScript: BCHscript): Boolean {
        for (o in tx.outputs) {
            if (o.script.contentEquals(outputScript)) {
                return true
            }
        }
        return false
    }

    /**
     * Check if transaction spends from address in tx
     */
    private suspend fun hasInputFrom(tx: BCHtransaction, address: PayAddress): Boolean {
        for (i in tx.inputs) {
            val txId = i.spendable.outpoint.txid
            val idx: Long = i.spendable.outpoint.getTransactionOrder()
            if (inputSpentTo(address, idx, txId)) {
                return true
            }
        }
        return false
    }

    /**
     * If input was spent to address
     */
    private suspend fun inputSpentTo(address: PayAddress, idx: Long, txId: Hash256): Boolean {
        val tx = getTransaction(txId)
        return tx.outputs[idx.toInt()].script.contentEquals(address.outputScript())
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectrumAPI? = null

        fun getInstance(chain: ChainSelector) =
            instance ?: synchronized(this) {
                instance ?: ElectrumAPI(chain).also { instance = it }
            }

        fun isInMempool(txHeight: Long): Boolean {
            return txHeight < 1
        }
    }
}
