package info.bitcoinunlimited.votepeer.vote

import bitcoinunlimited.libbitcoincash.BCHtransaction
import bitcoinunlimited.libbitcoincash.Hash256

sealed class VoteTransaction() {
    abstract val voteTx: BCHtransaction
    abstract val voteTxHash: Hash256
    abstract val voteTxId: String

    data class TwoOption(
        override val voteTx: BCHtransaction,
        override val voteTxHash: Hash256 = voteTx.hash,
        override val voteTxId: String = voteTxHash.toString()
    ) : VoteTransaction()

    data class Generic(
        override val voteTx: BCHtransaction,
        override val voteTxHash: Hash256 = voteTx.hash,
        override val voteTxId: String = voteTxHash.toString()
    ) : VoteTransaction()

    data class RingSignature(
        override val voteTx: BCHtransaction,
        val fundingTx: BCHtransaction,
        override val voteTxHash: Hash256 = voteTx.hash,
        override val voteTxId: String = voteTxHash.toString(),
    ) : VoteTransaction()
}
