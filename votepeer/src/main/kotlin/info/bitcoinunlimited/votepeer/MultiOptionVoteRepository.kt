package info.bitcoinunlimited.votepeer

import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.MultiOptionVote
import info.bitcoinunlimited.votepeer.election.*
import info.bitcoinunlimited.votepeer.tally.Tally
import info.bitcoinunlimited.votepeer.utils.TAG_MULTI_OPTION_VOTE
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteTransaction
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

private class CompareByTallyOrder {
    companion object : Comparator<Pair<Long, BCHtransaction>> {
        @OptIn(InternalCoroutinesApi::class)
        override fun compare(a: Pair<Long, BCHtransaction>, b: Pair<Long, BCHtransaction>): Int {
            val aHeight = if (ElectrumAPI.isInMempool(a.first)) { Int.MAX_VALUE } else { a.first.toInt() }
            val bHeight = if (ElectrumAPI.isInMempool(b.first)) { Int.MAX_VALUE } else { b.first.toInt() }

            if (aHeight != bHeight) return if (aHeight < bHeight) { -1 } else { 1 }
            return a.second.hash.compareTo(b.second.hash)
        }
    }
}

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@InternalCoroutinesApi
class MultiOptionVoteRepository(
    override val chain: ChainSelector,
    override val election: MultiOptionVoteElection,
    electionService: ElectionService,
    electrum: ElectrumAPI,
    identityRepository: IdentityRepository,
) : VoteRepository(identityRepository, electionService, electrum) {

    /**
     * The contract address *this* voter will cast his vote from.
     */
    private fun contractAddress(): PayAddress {
        val identity = identityRepository.getPkh()
        return election.getMultiOptionVote().voterContractAddress(chain, identity)
    }
    /**
     * Returns "lifecycle state" of vote. If voted, returns what was voted for, otherwise
     * returns if vote can be cast now.
     */
    override suspend fun getVoteStringHuman(): String {
        val voteTx = fetchOurVoteTransaction()

        if (voteTx != null) {
            val voteOptionHash = MultiOptionVote.parseVote(voteTx, election.getMultiOptionVote().voteOptionsHashed)
            return electionService.parseVoteSentence(election, voteOptionHash)
        }
        electrum.getLatestBlockHeight()?.let {
            return when (election.getElectionLifecycleState(it)) {
                ElectionLifecycle.ENDED -> "❌ You did not vote"
                ElectionLifecycle.NOT_STARTED -> "⌛ Vote when election starts"
                ElectionLifecycle.ONGOING -> "\uD83D\uDD34 You can vote now!"
            }
        }
        // Indeterminate, we don't know the blockchain tip height.
        return ""
    }

    override suspend fun getVoteOption(): VoteOption {
        val tx = fetchOurVoteTransaction() ?: return VoteOption(VoteOption.NOT_VOTED_MAGIC_INDEX)
        return ElectionService.getVoteOption(
            election,
            MultiOptionVote.parseVote(
                tx,
                election.getMultiOptionVote().voteOptionsHashed
            )
        )
    }

    override suspend fun getVoteTxId(): String {
        val tx: BCHtransaction = fetchOurVoteTransaction() ?: throw Error("No vote cast")
        return tx.hash.toString()
    }

    override suspend fun vote(option: VoteOption): Vote {
        val fundingAddress = identityRepository.currentUser.address!!
        val fundingSecret = identityRepository.currentUser.secret!!
        val votingSecret = identityRepository.currentUser.secret

        val coin = electrum.getOrCreateUtxo(
            contractAddress(),
            fundingAddress,
            MultiOptionVote.castingTxSize(chain),
            fundingSecret,
            votingSecret
        )

        val castInput = BCHinput(chain, coin, BCHscript(chain))
        castInput.spendable.secret = votingSecret
        val voteTx = election.getMultiOptionVote().castVote(
            chain,
            castInput,
            fundingAddress /* change address */,
            election.getOptionHash(option.selected)
        )
        electrum.broadcast(voteTx)
        return Vote(option, VoteTransaction.Generic(voteTx))
    }

    override suspend fun getTally(publicKeyHash: ByteArray): Tally {
        val voters = election.participants.map { participantPkh ->
            val voterAddress = PayAddress(chain, PayAddressType.P2PKH, participantPkh).toString()
            val contractAddress = election.getMultiOptionVote().voterContractAddress(chain, participantPkh)
            Pair(voterAddress, contractAddress)
        }

        val option: MutableMap<String, MutableList<Pair<String?, String?>>> = mutableMapOf()

        // Initiate tally
        for (o in election.options) {
            option[o] = mutableListOf()
        }
        option[VoteOption.NOT_VOTED_MAGIC_INDEX] = mutableListOf()

        for ((voterAddress, contractAddress) in voters) {
            try {
                val (_, voteTx) = fetchVoteTransaction(contractAddress) ?: Pair(null, null)
                if (voteTx != null) {
                    val voteHash = MultiOptionVote.parseVote(voteTx, election.getMultiOptionVote().voteOptionsHashed)
                    val optionIndex = ElectionService.getVoteOption(election, voteHash)
                    option[optionIndex.selected]!!.add(Pair(voteTx.hash.toString(), voterAddress))
                } else {
                    // User has not vote yet.
                    option[VoteOption.NOT_VOTED_MAGIC_INDEX]!!.add(Pair(null, voterAddress))
                }
            } catch (e: Exception) {
                Log.w(TAG_MULTI_OPTION_VOTE, e)
            }
        }
        return Tally(election.id, option)
    }

    /**
     * If, and only if, the transaction is confirmed can it be cached.
     * To invalidate the cache after confirmation would require a block reorg + double spend.
     */
    private var cachedVoteTransaction: BCHtransaction? = null
    /**
     * Fetch vote transaction for THIS user.
     */
    private suspend fun fetchOurVoteTransaction(): BCHtransaction? {

        cachedVoteTransaction?.let {
            return it
        }
        val (height, tx) = fetchVoteTransaction(contractAddress()) ?: return null

        if (!ElectrumAPI.isInMempool(height)) {
            cachedVoteTransaction = tx
        }
        return tx
    }

    /**
     * Fetch vote transaction for any voter.
     */
    private suspend fun fetchVoteTransaction(contractAddress: PayAddress): Pair<Long, BCHtransaction>? {
        return electrum.getSpendingTransactions(
            contractAddress,
            election.beginHeight,
            election.endHeight
        ).sortedWith(CompareByTallyOrder).firstOrNull()
    }
}
