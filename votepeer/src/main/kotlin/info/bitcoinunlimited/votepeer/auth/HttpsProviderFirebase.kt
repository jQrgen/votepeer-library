package info.bitcoinunlimited.votepeer.auth

import com.google.firebase.functions.FirebaseFunctions
import info.bitcoinunlimited.votepeer.utils.Constants
import kotlinx.coroutines.tasks.await

abstract class HttpsProvider {
    abstract suspend fun getChallenge(): String
}
object HttpsProviderFirebase : HttpsProvider() {

    override suspend fun getChallenge(): String {
        val task = FirebaseFunctions.getInstance(Constants.region)
            .getHttpsCallable(Constants.REQUEST_CHALLENGE)
            .call()
        val result = task.await()

        return result.data as String
    }
}

object HttpsProviderFirebaseFaker : HttpsProvider() {
    override suspend fun getChallenge(): String { return "challenge-fake" }
}