package info.bitcoinunlimited.votepeer.election.master

import info.bitcoinunlimited.votepeer.ElectrumApiConnectionState
import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface ElectionMasterView {
    /**
     * Intent to load the current ElectionMaster state
     *
     * @return A flow that inits the current ElectionMasterViewState
     */
    fun initState(): Flow<Event<Boolean>>

    fun initElectionsState(): Flow<Event<Boolean>>

    fun refreshSwiped(): Flow<Event<ElectionMasterViewIntent.SwipeToRefresh?>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: ElectionMasterViewState)

    /**
     * Renders elections
     *
     * @param state The current elections to display
     */
    fun renderElections(state: ElectionsViewState)

    /**
     * Intent to load the current electrumApiState
     *
     * @return A flow that inits the current electrumApiState
     */
    fun initElectrumApiState(): Flow<Event<Boolean>>

    /**
     * Renders the electrumApiState
     *
     * @param state The current electrumApiState display
     */
    fun renderElectrumApiState(state: ElectrumApiConnectionState)
}
