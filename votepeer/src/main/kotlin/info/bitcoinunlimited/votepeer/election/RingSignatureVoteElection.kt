package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.vote.RingSignatureVote
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import java.io.Serializable

data class RingSignatureVoteElection(
    val electionRaw: ElectionRaw,
    override val participants: Array<ByteArray>
) : Serializable, Election(electionRaw) {
    init {
        if (participants.isEmpty())
            throw IllegalArgumentException("election.participants.isEmpty()")
    }
    override fun getContractElectionID(): ByteArray {
        if (participants.isEmpty())
            throw IllegalArgumentException("getContractElectionID: election.participants.isEmpty()2")
        return RingSignatureVote(
            electionRaw.salt.toByteArray(),
            electionRaw.description,
            electionRaw.beginHeight.toInt(),
            electionRaw.endHeight.toInt(),
            electionRaw.options,
            participants
        ).getElectionID()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RingSignatureVoteElection

        if (electionRaw != other.electionRaw) return false
        if (!participants.contentDeepEquals(other.participants)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = electionRaw.hashCode()
        result = 31 * result + participants.contentDeepHashCode()
        return result
    }
}