package info.bitcoinunlimited.votepeer.election.network

import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.MultiOptionVoteElection
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.election.TwoOptionVoteElection
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.utils.TAG_REST_PROVIDER
import io.ktor.client.request.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.tasks.await

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
@InternalCoroutinesApi
// TODO: Move to ElectionRepository
object ElectionProviderRest {
    internal suspend fun getLinkedKeys(electionId: String): ArrayList<String> {
        val mUser: FirebaseUser = FirebaseAuth.getInstance().currentUser ?: throw IllegalStateException("no user")
        val idToken = mUser.getIdToken(true).await().token ?: throw IllegalStateException("Not Authenticated")
        val authorization = "Bearer $idToken"
        try {
            val httpclient = ElectionService.createHttpClient()
            val result: Map<String, String> = httpclient.get("${Constants.EndpointUrl}get_linked_keys") {
                parameter("election_id", electionId)
                header("authorization", authorization)
            }
            val keys = arrayListOf<String>()
            keys.addAll(result.values)
            return keys
        } catch (exception: Exception) {
            Log.e(TAG_REST_PROVIDER, exception.message ?: "Something went wrong when fetching participant keys")
            throw exception
        }
    }

    internal suspend fun getElectionsRaw(): List<ElectionRaw> {
        val mUser: FirebaseUser = FirebaseAuth.getInstance().currentUser ?: throw IllegalStateException("no user")
        val idToken = mUser.getIdToken(true).await().token ?: throw IllegalStateException("Not Authenticated")
        val authorization = "Bearer $idToken"
        val httpclient = ElectionService.createHttpClient()
        val electionResult: ListParticipatingElectionsResult = httpclient.get(
            "${Constants.EndpointUrl}list_participating_elections"
        ) {
            header("authorization", authorization)
        }
        return electionResult.elections
    }

    internal fun getMultiOptionElection(electionRaw: ElectionRaw): MultiOptionVoteElection {
        val participants = UtilVote.sortByteArray(
            electionRaw.participantAddresses.map {
                PayAddress(it).data
            }.toTypedArray()
        )
        return info.bitcoinunlimited.votepeer.election.MultiOptionVoteElection(electionRaw, participants)
    }

    internal fun getTwoOptionElection(electionRaw: ElectionRaw): TwoOptionVoteElection {
        val participants = UtilVote.sortByteArray(
            electionRaw.participantAddresses.map {
                // TODO: Check if this does the same as UtilStringEncoding.hexToByteArray(it)
                PayAddress(it).data
            }.toTypedArray()
        )
        return TwoOptionVoteElection(electionRaw, participants)
    }

    internal suspend fun getRingSignatureElection(
        electionRaw: ElectionRaw,
    ): RingSignatureVoteElection {
        val participantsHex = getLinkedKeys(electionRaw.id)
        val participants = UtilVote.sortByteArray(
            participantsHex.map {
                // TODO: Check if this does the same as PayAddress(it).data
                UtilStringEncoding.hexToByteArray(it)
            }.toTypedArray()
        )
        return RingSignatureVoteElection(electionRaw, participants)
    }

    internal suspend fun parseElection(electionRaw: ElectionRaw): Election {

        return when (val contractType = electionRaw.contractType) {
            Constants.RING_SIGNATURE_ELECTION -> {
                // TODO: Run in parallel?
                getRingSignatureElection(electionRaw)
            }
            Constants.MULTI_OPTION_ELECTION -> {
                getMultiOptionElection(electionRaw)
            }
            Constants.TWO_OPTION_ELECTION -> {
                getTwoOptionElection(electionRaw)
            }
            else -> {
                throw Exception("Cannot render election for ContractType $contractType")
            }
        }
    }

    // TODO: Support more than 100 elections (use cursor to fetch more)
    internal suspend fun getElections(): List<Election> {
        try {
            val electionsRaw = getElectionsRaw()
            val elections = mutableListOf<Election>()
            electionsRaw.forEach { electionRaw ->
                val election = parseElection(electionRaw)
                elections.add(election)
            }

            return elections
        } catch (exception: Exception) {
            Log.e(TAG_REST_PROVIDER, exception.toString())
            throw exception
        }
    }

    internal suspend fun getElectionRaw(electionId: String): ElectionRaw {
        val mUser: FirebaseUser = FirebaseAuth.getInstance().currentUser ?: throw IllegalStateException("no user")
        val idToken = mUser.getIdToken(true).await().token ?: throw IllegalStateException("Not Authenticated")
        val authorization = "Bearer $idToken"
        val httpclient = ElectionService.createHttpClient()
        val electionRaw: ElectionRaw = httpclient.get("${Constants.EndpointUrl}get_election_details") {
            parameter("election_id", electionId)
            header("authorization", authorization)
        }
        return electionRaw
    }

    @Suppress("unused")
    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    suspend fun getElection(electionID: String): Election {
        try {
            val electionRaw = getElectionRaw(electionID)
            return parseElection(electionRaw)
        } catch (e: Exception) {
            Log.e(TAG_REST_PROVIDER, e.message ?: e.toString())
            throw Exception("Something went wrong in getElectionNetwork, electionID: $electionID. ${e.message}")
        }
    }
}