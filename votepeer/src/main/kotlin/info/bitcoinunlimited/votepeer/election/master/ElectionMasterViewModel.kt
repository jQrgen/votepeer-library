package info.bitcoinunlimited.votepeer.election.master

import android.app.Application
import android.util.Log
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.ChainSelector
import info.bitcoinunlimited.votepeer.*
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.election.*
import info.bitcoinunlimited.votepeer.election.master.ElectionMasterViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.election.network.ElectionProviderRest
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.ringSignatureVote.room.RingSignatureVoteDatabase
import info.bitcoinunlimited.votepeer.twoOptionVote.TwoOptionVoteRepository
import info.bitcoinunlimited.votepeer.utils.TAG_ELECTION_MASTER
import info.bitcoinunlimited.votepeer.utils.onEachEvent
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModel(
    private val electionService: ElectionService,
    private val authRepository: AuthRepository,
    private val identityRepository: IdentityRepository,
    private val electrumApi: ElectrumAPI,
    private val restProviderElection: ElectionProviderRest,
    private val chain: ChainSelector,
    app: Application
) : AndroidViewModel(app) {
    private var latestBlockHeight = 0L
    private val _electionsState = MutableStateFlow<ElectionsViewState?>(null)
    private val _state = MutableStateFlow<ElectionMasterViewState?>(null)
    internal val state = _state.asStateFlow()
    private val electrumConnection = MutableStateFlow<ElectrumApiConnectionState?>(null)
    private val voteRepositories = mutableMapOf<String, VoteRepository>()

    internal val handler = CoroutineExceptionHandler { context, throwable ->
        val exception = Exception(throwable)
        Log.e(TAG_ELECTION_MASTER, context.toString())
        Log.e(TAG_ELECTION_MASTER, throwable.message ?: "Something went wrong in ElectionMasterFragment")
        setViewState(LoadingSpinner(false))
        setViewState(ErrorElectionMaster(exception))
    }

    init {
        viewModelScope.launch(Dispatchers.IO + handler) {
            fetchElections()
        }
        observeElectrumApiStatus()
        observeAuthState()
    }

    internal fun bindIntents(view: ElectionMasterView) {
        view.initState().onEach {
            _state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope + handler)

        view.initElectionsState().onEach {
            _electionsState.filterNotNull().collect {
                view.renderElections(it)
            }
        }.launchIn(viewModelScope + handler)

        view.initElectrumApiState().onEach {
            electrumConnection.filterNotNull().collect {
                view.renderElectrumApiState(it)
            }
        }.launchIn(viewModelScope + handler)

        view.refreshSwiped().onEachEvent { _ ->
            viewModelScope.launch(Dispatchers.IO + handler) {
                fetchElections()
            }
        }.launchIn(viewModelScope + handler)
    }

    internal suspend fun fetchElections() {
        setViewState(LoadingSpinner(true))
        val elections = fetchElectionsNetworkIfAuthenticated(authRepository.authState.value) ?: return

        elections.forEach { bindRepository(it) }
        elections.map {
            it.voteSentence = voteRepositories[it.id]?.getVoteStringHuman()
                ?: "ERROR: Cannot getVoteStringHuman in mapElectionsToNetworkMetadata"
        }

        setElectionsViewState(ElectionsViewState(elections))
        setViewState(LoadingSpinner(false))
    }

    internal fun bindRepository(election: Election) {
        if (voteRepositories.containsKey(election.id)) {
            return
        }
        voteRepositories[election.id] = when (election) {
            is RingSignatureVoteElection -> RingSignatureVoteRepository(
                election,
                electionService,
                electrumApi,
                RingSignatureVoteDatabase.getInstance(getApplication()).ringSignatureVoteDao(),
                identityRepository,
            )
            is TwoOptionVoteElection -> TwoOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumApi,
                identityRepository
            )
            is MultiOptionVoteElection -> MultiOptionVoteRepository(
                chain,
                election,
                electionService,
                electrumApi,
                identityRepository
            )
            else -> throw NotImplementedError(
                "Election type not supported in bindRepository"
            )
        }
    }

    internal suspend fun fetchBlockHeight(): Long {
        electrumApi.getLatestBlockHeight()?.let {
            latestBlockHeight = it
            return it
        }
        return latestBlockHeight
    }

    internal fun observeAuthState() = viewModelScope.launch(Dispatchers.IO + handler) {
        authRepository.authState.value.let {
            setViewState(Auth(it))
            if (it is AuthState.AuthenticatedKeyPair)
                fetchElections()
        }
        authRepository.authState.collect {
            setViewState(Auth(it))
            if (it is AuthState.AuthenticatedKeyPair)
                fetchElections()
        }
    }

    fun observeElectrumApiStatus() = viewModelScope.launch(Dispatchers.IO + handler) {
        electrumApi.connectionState.value.let {
            setElectrumConnectionViewState(it)
        }
        electrumApi.connectionState.collect { connectionState ->
            setElectrumConnectionViewState(connectionState)
            electrumApi.ping()
        }
    }

    internal fun setViewState(
        viewState: ElectionMasterViewState
    ) = viewModelScope.launch(Dispatchers.Main) {
        _state.value = viewState
    }

    internal fun setElectionsViewState(
        viewState: ElectionsViewState
    ) = viewModelScope.launch(Dispatchers.Main) {
        _electionsState.value = viewState
    }

    internal fun setElectrumConnectionViewState(
        viewState: ElectrumApiConnectionState
    ) = viewModelScope.launch(Dispatchers.Main) {
        Log.i(TAG_ELECTION_MASTER, viewState.toString())
        electrumConnection.value = viewState
    }

    // TODO: Move to ElectionRepository?
    internal suspend fun fetchElectionsNetworkIfAuthenticated(authState: AuthState): List<Election>? {
        if (authState is AuthState.AuthenticatedKeyPair) {
            val electionsNetwork = restProviderElection.getElections()
            return electionsNetwork.sortedBy { it.endHeight }
        }
        return null
    }
}
