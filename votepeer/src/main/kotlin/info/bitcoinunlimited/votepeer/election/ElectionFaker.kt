package info.bitcoinunlimited.votepeer.election

import bitcoinunlimited.libbitcoincash.*
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVote
import bitcoinunlimited.libbitcoincash.vote.TwoOptionVoteContract
import info.bitcoinunlimited.votepeer.election.network.ElectionRaw
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteTransaction
import kotlin.random.Random

@Suppress("unused")
object ElectionFaker {
    // NOTE: Do not change these without verifying that all tests run first.
    // This is basically the default test-case
    val electionRawMock = ElectionRaw(
        "mock_id",
        "bitcoincash:qp94x7qc0uh4jex8qqfghwcf37ju2nvp9q5d8rw0s0",
        "BUIP169: Add support for larger integers\nSubmitted by: Jonathan Silverblood\nDate: 2021/06/05\nSummary\nBitcoin " +
            "Unlimited should take a positive public stance towards supporting larger integers in Bitcoin Cash (BCH) script.\n" +
            "Proposal\nBitcoin unlimited should: Publicly announce that they are in favor of supporting some proposal that allows " +
            "larger integers in Bitcoin Cash (BCH) script.\nHelp set up, or contribute to, a testnet where the larger integers can be " +
            "demonstrated before the end of August.\nHelp finalize a specification document based on, or as part of, a proposal to " +
            "allow larger integers in Bitcoin Cash (BCH) before the end of October. For example, they could support " +
            "CHIP-2021-02-Bigger-Script-Integers.md · master · GeneralProtocols / Research / CHIPs · GitLab 1 Motivation Bitcoin " +
            "Unlmited benefits from being an active participant in the Bitcoin Cash (BCH) ecosystem, and should be proactive and take " +
            "part in its development.\nBackground There are people in Bitcoin Unlimited who believe that it is getting stagnant, " +
            "partly due to lack of BUIPs and BUIP engagement. It is undeniably the case that both of these metrics have gone down, but " +
            "it is not clear what the reason for this result is. Budget This proposal should fit within existing approved budgets for " +
            "Bitcoin Unlimited development.",
        arrayOf(),
        "Oh yes or oh no?",
        400L,
        Constants.MULTI_OPTION_ELECTION,
        "",
        arrayOf("Oh yes!", "Oh no!"),
        arrayOf(),
        300L
    )

    val participants = mutableListOf(
        "bitcoincash:aaa8a14f0658c44809580b24299a592d7623976c",
        "bitcoincash:3790bb07029831ec90f8eb1ed7c1c09aac178185",
        "bitcoincash:aaa8a14f0658c44809580b24299a592d7623976c",
        "bitcoincash:9b1772d9287b9a3587b0a1e147f34714697c780c",
        "bitcoincash:ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004",
        "bitcoincash:f7d1358c4baa20b31e5f84c238e964473d733f75",
    )

    fun generateOngoingTwoOptionElection(): TwoOptionVoteElection {
        return generateOngoingTwoOptionElection("mock_id")
    }

    fun generateOngoingTwoOptionElection(currentHeight: Long): TwoOptionVoteElection {
        val beginHeight = currentHeight - 100L
        val endHeight = currentHeight + 100L
        return generateOngoingTwoOptionElection("mock_id").copy(electionRawMock.copy(beginHeight = beginHeight, endHeight = endHeight))
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun generateOngoingRawElection(id: String): ElectionRaw {
        val beginHeightMock = Random.nextLong(101L, 1000L)
        val endHeightMock = Random.nextLong(beginHeightMock + 1L, 10000L)

        return ElectionRaw(
            id,
            "bitcoincash:qp94x7qc0uh4jex8qqfghwcf37ju2nvp9q5d8rw0s0",
            "Description!!!" + Random.nextLong(101L, 10000000L).toString(),
            participants.toTypedArray(),
            "Yes or no?" + Random.nextLong(101L, 10000000L),
            endHeightMock,
            Constants.MULTI_OPTION_ELECTION,
            "",
            arrayOf("Yes", "No"),
            arrayOf(),
            beginHeightMock
        )
    }

    fun generateOngoingTwoOptionElection(id: String): TwoOptionVoteElection {
        return TwoOptionVoteElection(generateOngoingRawElection(id), arrayOf())
    }

    fun generateOngoingRingSignatureElection(id: String): RingSignatureVoteElection {
        val participantsMock = participants.map { ByteArray(32) }
        return RingSignatureVoteElection(generateOngoingRawElection(id), participantsMock.toTypedArray())
    }

    fun generateTwoOptionElections(amount: Int): List<TwoOptionVoteElection> {
        val elections = mutableListOf<TwoOptionVoteElection>()
        for (i in 1..amount) {
            elections.add(generateOngoingTwoOptionElection(i.toString()))
        }
        return elections.toList()
    }

    fun createTwoOptionElectionWithAbstainVote(): Election {
        val election = generateOngoingTwoOptionElection("id_mock")
        return election
    }

    fun bchInput(chain: ChainSelector, privateKey: ByteArray, amount: Long): BCHinput {
        val publicKey = PayDestination.GetPubKey(privateKey)
        val pkh = Hash.hash160(publicKey)
        val input = BCHinput(chain)
        val payAddress = PayAddress(chain, PayAddressType.P2PKH, pkh)
        input.spendable.priorOutScript = payAddress.outputScript()
        input.spendable.amount = amount
        input.spendable.secret = UnsecuredSecret(privateKey)
        return input
    }

    @Suppress("unused")
    fun mockSignature(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }
    fun mockPrivateKey(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }

    @Suppress("MemberVisibilityCanBePrivate")
    fun outputToInput(
        chain: ChainSelector,
        tx: BCHtransaction,
        outputIndex: Int,
        secret: ByteArray
    ): BCHinput {
        val input = BCHinput(chain)
        input.spendable.amount = tx.outputs[outputIndex].amount
        input.spendable.outpoint = BCHoutpoint(tx.calcHash(), outputIndex.toLong())
        input.spendable.secret = UnsecuredSecret(secret)
        return input
    }

    fun mockTwoOptionVoteContracts(optA: String, optB: String, salt: ByteArray): List<TwoOptionVoteContract> {
        val description = "Foo?"
        val optAHash = UtilVote.hash160Salted(salt, optA.toByteArray())
        val optBHash = UtilVote.hash160Salted(salt, optB.toByteArray())
        val endHeight = 642042
        val participants = arrayOf(
            UtilStringEncoding.hexToByteArray("0e2f5d8a017c4983cb56320d18f66bf01587aa44"),
            UtilStringEncoding.hexToByteArray("3790bb07029831ec90f8eb1ed7c1c09aac178185"),
            UtilStringEncoding.hexToByteArray("aaa8a14f0658c44809580b24299a592d7623976c"),
            UtilStringEncoding.hexToByteArray("9b1772d9287b9a3587b0a1e147f34714697c780c"),
            UtilStringEncoding.hexToByteArray("ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004"),
            UtilStringEncoding.hexToByteArray("f7d1358c4baa20b31e5f84c238e964473d733f75")
        )

        val id = TwoOptionVote.calculate_proposal_id(
            salt, description, optA, optB, endHeight, participants
        )

        return participants.map { TwoOptionVoteContract(id, optAHash, optBHash, it) }
    }

    fun getVoteTxTwoOptionVote(vote: String, salt: String, optA: String, optB: String): VoteTransaction.TwoOption {
        val voteHash = UtilVote.hash160Salted(salt.toByteArray(), vote.toByteArray())
        val privateKey = mockPrivateKey()
        val contract = mockTwoOptionVoteContracts(optA, optB, salt.toByteArray())[0]
        val chain = ChainSelector.BCHMAINNET
        val changeAddress: PayDestination = Pay2PubKeyHashDestination(
            chain, UnsecuredSecret(mockPrivateKey('B'))
        )

        val inputAmount = (TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE)

        val fundCoin = bchInput(chain, privateKey, inputAmount)
        val fundTx = contract.fundContract(
            chain, fundCoin,
            changeAddress
        )

        val contractCoin = outputToInput(chain, fundTx, 0, privateKey)
        return VoteTransaction.TwoOption(
            contract.castVote(
                chain, contractCoin,
                changeAddress,
                voteHash
            )
        )
    }

    private const val optionA = "yes"
    private const val optionB = "no"
    private const val salt = "unittest"

    private const val OPTION_A = 0
    private const val OPTION_B = 1

    fun getVoteOptionA(election: Election): VoteOption {
        val selectedOption = election.options[OPTION_A]
        val voteTx = getVoteTxTwoOptionVote(selectedOption, salt, election.options[OPTION_A], election.options[OPTION_B])
        return VoteOption("OPTION_A")
    }

    fun getVoteOptionB(election: Election): VoteOption {
        val selectedOption = election.options[OPTION_B]
        val voteTx = getVoteTxTwoOptionVote(selectedOption, salt, election.options[OPTION_A], election.options[OPTION_B])
        return VoteOption("OPTION_B")
    }

    fun getAbstainVoteTransactions(salt: String, optA: String, optB: String): BCHtransaction {
        val privateKey = mockPrivateKey()
        val contract = mockTwoOptionVoteContracts(optA, optB, salt.toByteArray())[0]
        val chain = ChainSelector.BCHMAINNET
        val changeAddress: PayDestination = Pay2PubKeyHashDestination(
            chain, UnsecuredSecret(mockPrivateKey('B'))
        )

        val inputAmount = (TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE)

        val fundCoin = bchInput(chain, privateKey, inputAmount)
        val fundTx = contract.fundContract(
            chain, fundCoin,
            changeAddress
        )

        val contractCoin = outputToInput(chain, fundTx, 0, privateKey)
        return contract.castVote(
            chain, contractCoin,
            changeAddress,
            TwoOptionVoteContract.BLANK_VOTE
        )
    }

    fun getAbstainVoteTransactionTwoOption(salt: String, optA: String, optB: String): VoteTransaction.TwoOption {
        val privateKey = mockPrivateKey()
        val contract = mockTwoOptionVoteContracts(optA, optB, salt.toByteArray())[0]
        val chain = ChainSelector.BCHMAINNET
        val changeAddress: PayDestination = Pay2PubKeyHashDestination(
            chain, UnsecuredSecret(mockPrivateKey('B'))
        )

        val inputAmount = (TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE)

        val fundCoin = bchInput(chain, privateKey, inputAmount)
        val fundTx = contract.fundContract(
            chain, fundCoin,
            changeAddress
        )

        val contractCoin = outputToInput(chain, fundTx, 0, privateKey)
        return VoteTransaction.TwoOption(
            contract.castVote(
                chain, contractCoin,
                changeAddress,
                TwoOptionVoteContract.BLANK_VOTE
            )
        )
    }

    fun getAbstainBallot(): VoteOption {
        val voteTx = getAbstainVoteTransactions(salt, optionA, optionB)
        return VoteOption("OPTION_B")
    }
}
