package info.bitcoinunlimited.votepeer.election.fund

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class FundVoteIntent(
    val initExceptionState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val initSlideToVoteState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val initHasVotedState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
)
