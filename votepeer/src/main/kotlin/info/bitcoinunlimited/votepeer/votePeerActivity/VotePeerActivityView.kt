package info.bitcoinunlimited.votepeer.votePeerActivity

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface VotePeerActivityView {
    /**
     * Intent to load the current MainActivity state
     *
     * @return A flow that inits the current MainActivityViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Intent to submit the connection status
     *
     * @return A flow that emits the connections status
     */
    fun submitConnectionStatus(): Flow<Event<VotePeerActivityViewState.ConnectionStatus>>

    fun qrCodeRead(): Flow<Event<VotePeerActivityViewIntent.QrCodeRead?>>

    /**
     * Renders the MainActivityViewState
     *
     * @param state The current view state display
     */
    fun render(state: VotePeerActivityViewState)
}
