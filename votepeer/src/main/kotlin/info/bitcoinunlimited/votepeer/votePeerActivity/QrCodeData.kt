package info.bitcoinunlimited.votepeer.votePeerActivity

import java.net.URI

data class QrCodeData(
    val rawUri: String,
    val op: String = generateFrom(rawUri).op,
    val chal: String = generateFrom(rawUri).chal,
    val cookie: String = generateFrom(rawUri).cookie
) {

    companion object {
        // TODO: Test
        private fun generateFrom(rawUri: String): QrCodeData {
            lateinit var op: String
            lateinit var chal: String
            lateinit var cookie: String
            val uri = URI(rawUri)
            val rawQuery = uri.rawQuery
            val queries = rawQuery.split("&")

            queries.forEach { query ->
                val querySplit = query.split("=")
                val queryParameter = querySplit[0]
                val queryValue = querySplit[1]

                when (queryParameter) {
                    "op" -> op = queryValue
                    "chal" -> chal = queryValue
                    "cookie" -> cookie = queryValue
                }
            }

            return QrCodeData(
                rawUri,
                op,
                chal,
                cookie
            )
        }
    }
}
