package info.bitcoinunlimited.votepeer.votePeerActivity

import info.bitcoinunlimited.votepeer.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class VotePeerActivityViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val qrCodeRead: MutableStateFlow<Event<QrCodeRead?>> = MutableStateFlow(Event(null)),
    val submitConnectionStatus: MutableStateFlow<Event<VotePeerActivityViewState.ConnectionStatus>> =
        MutableStateFlow(Event(VotePeerActivityViewState.ConnectionStatus(true)))
) {
    data class QrCodeRead(
        val uri: String
    )
}
